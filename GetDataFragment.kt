package com.example.rxjava

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.rxjava.databinding.FragmentGetDataBinding
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import java.io.InputStream

class GetDataFragment : Fragment() {

    private lateinit var binding: FragmentGetDataBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGetDataBinding.inflate(inflater)

        return binding.root
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            btnGetDataJSON.setOnClickListener {
                checkJSON()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        tvJsonData.text = it
                    }
            }
        }
    }

    fun checkJSON(): Observable<String> {
        var json: String?
        var jsonArray: JSONArray

        return Observable.create { subscriber ->
            val inputStream: InputStream? = activity?.assets?.open("names.json")
            json = inputStream?.bufferedReader().use { it?.readText() }
            jsonArray = JSONArray(json)
            for (i in 0 until jsonArray.length()) {
                subscriber.onNext(jsonArray.getString(i))
                Thread.sleep(1000L)
            }
            subscriber.onComplete()
        }

    }
}